## Challenges

**Requirements:**
- Allow multiple interfaces on the same instance (could be on the same or different network).
- Attach FIP, specifically to `eth0` interface.

**Issues:**
- The HOT that is passed to Heat goes through n layers of checking, validation, manipulation and building and rebuilding process.<br/>
    database_dummy -> ContextConfigurables -> _NVMsDiffNW_ -> HeatContext -> models.Server -> HeatTemplate **->** HeatContext -> HeatTemplate -> ...
- Tracing `_add_port()` (another critical function):<br/>
    _add_instance() <- add_to_template() <- _add_resources_to_template() <- _create_template() <- _initialise_stack() <- deploy() <- create_and_deploy() <- Engine
- model.py:335

## Differences




[**Version: 2013-05-23**](http://docs.openstack.org/developer/heat/juno/template_guide/openstack.html#OS::Nova::FloatingIPAssociation)

```yaml
heat_template_version: 2013-05-23
...
resources:
  ...
  the_resource:
    type: OS::Nova::FloatingIPAssociation
    properties:
      floating_ip: String
      server_id: String
```

[**Version: 2015-04-30**](http://docs.openstack.org/developer/heat/template_guide/openstack.html#OS::Neutron::FloatingIPAssociation)
```yaml
heat_template_version: 2015-04-30
...
resources:
  ...
  the_resource:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      fixed_ip_address: String
      floatingip_id: String
      port_id: String
```

Better still:

```yaml
heat_template_version: 2015-04-30
...
resources:
  ...
  the_resource:
    type: OS::Neutron::FloatingIP
    properties:
      dns_domain: String
      dns_name: String
      fixed_ip_address: String
      floating_ip_address: String
      floating_network: String
      port_id: String
      value_specs: {...}
```


|    Old version                        |     New version        |
|:--------------------------------------|:----------------------:|
| Create FIP, and then associate it with a server.  | Create FIP and then associate it with a port, or just associate directly while creation. |
| No guarantee which port the FIP gets associated with. | Total control. |



## New Approach

Forget new contexts. Add support for a configurable number of interfaces in the existing contexts.

The only feasible option: add multiple ports for the same network. ve_engines/orchestrator/model.py:335
